package oop.ex6.SymbolTable;

import oop.ex6.Variable.Variable;

import java.util.HashMap;

public class SymbolTable {

	private SymbolTable father;
	private HashMap<String, Variable> variables;

	private boolean inTable(String varName){
		return variables.containsKey(varName);
	}

	public void insert(String varName, Variable var){
		variables.putIfAbsent(varName, var);
	}

	public Variable getVariable(String varName) throws VariableNotDeclaredException{
		Variable var = variables.get(varName);
		if (var == null){
			if (father != null) {
				return father.getVariable(varName);
			}
			else {
				throw new VariableNotDeclaredException();
			}
		}
		else {
			return var;
		}
	}
}
