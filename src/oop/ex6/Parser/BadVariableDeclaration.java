package oop.ex6.Parser;

public class BadVariableDeclaration extends CompilationException {
	public BadVariableDeclaration(){
		super("bad variable declaration format\n");
	}
}
