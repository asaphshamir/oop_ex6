package oop.ex6.Parser;

public class FinalAssignmentException extends CompilationException {
	public FinalAssignmentException(String varName){
		super("cannot assign to final variable: " + varName);
	}
}
