package oop.ex6.Parser;

public class CompilationException extends Exception {
	public CompilationException(String message){
		super(message);
	}

	public CompilationException(){
		super();
	}
}
